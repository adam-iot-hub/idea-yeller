# Yell your Idea

Your great ideas evaporate by the time you find a pen. Now you just need to bash the MF button and yell it out to make sure your ideas are forever engraved into the fabric of time (and thermo).

Made for a rasp-pi with a usb mic and a button attached to GPIO.

### Installing
Install rpi.gpio
```
sudo apt-get install python-rpi.gpio python3-rpi.gpio
```

Install pyaudio `sudo apt-get install python-pyaudio python3-pyaudio`, then:
```
pip install -r requirements.txt
```
If you want to use PocketSphinx as a local recognition, install it following [this guide](https://howchoo.com/g/ztbhyzfknze/how-to-install-pocketsphinx-on-a-raspberry-pi). Then:
```
pip install pocketsphinx
```