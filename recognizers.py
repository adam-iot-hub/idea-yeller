def google(audio, r):
    return r.recognize_google(audio)


def sphinx(audio, r):
    return r.recognize_sphinx(audio)
