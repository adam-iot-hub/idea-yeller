import speech_recognition as sr
import recognizers
import Adafruit_Thermal


config = {
    'mic_i': 0,
    'recognize': recognizers.google,  # Note: google recognizer is limited to 50/day
    'printer': {
        'port': '/dev/serial0',
        'baud': 9600
    },
    'button_pin': 10,
    'trigger': {
        'activator': 'keyword',  # button or keyword (keyword requires snowboy)
        'keywords': ['snowboy/computer.pmdl'],  # directory to pmdl files for keywords
        # 'snowboy/Discovery.pmdl', 'snowboy/Discovery2.pmdl', 'snowboy/Discovery3.pmdl', 'snowboy/Discovery4.pmdl',
        'snowboy_dir': 'snowboy/'
    }
}


def main():
    print("microphones:")
    print(sr.Microphone.list_microphone_names())
    print("Using: %s" % (sr.Microphone.list_microphone_names()[config['mic_i']]))

    if config['trigger']['activator'] == 'button':
        import RPi.GPIO as GPIO
        # Setup GPIO button
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(config['button_pin'], GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.add_event_detect(config['button_pin'], GPIO.RISING, callback=listen_and_print)

        print("waiting for button press")

        while True:
            pass

    if config['trigger']['activator'] == 'keyword':
        print("Listening to keywords %s" % (config['trigger']['keywords']))
        r = sr.Recognizer()

        while True:
            with sr.Microphone(device_index=config['mic_i']) as source:
                audio = r.listen(source, snowboy_configuration=(config['trigger']['snowboy_dir'], config['trigger']['keywords']), phrase_time_limit=2)
            print("got it")

            # Try to recognize audio
            try:
                text = config['recognize'](audio, r)
                # text = text.split(' ', 1)[1]
                print(">" + text)
                # print_text(text)
            except sr.UnknownValueError:
                print("> [Unknown]")
            except sr.RequestError as e:
                print("Error: {0}".format(e))


def listen_and_print():
    r = sr.Recognizer()

    with sr.Microphone(device_index=config['mic_i']) as source:
        audio = r.listen(source)

    # Try to recognize audio
    try:
        text = config['recognize'](audio, r)
        print(">" + text)
        print_text(text)
    except sr.UnknownValueError:
        print("> [Unknown]")
    except sr.RequestError as e:
        print("Error: {0}".format(e))


def print_text(text):
    printer = Adafruit_Thermal.Adafruit_Thermal(config['printer']['port'], config['printer']['baud'], timeout=5)

    printer.feed(2)
    printer.setSize('L')
    printer.write(text)
    printer.feed(2)


if __name__ == '__main__':
    main()
