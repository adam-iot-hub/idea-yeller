default:
	python app.py

activate_fish:
	source env/bin/activate.fish

clean:
	rm recordings/*